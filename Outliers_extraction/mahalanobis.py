import pandas as pd
import numpy as np


def mahalanobis(x, mean=None, cov=None, data=None):
    """
    Compute the Mahalanobis Distance for a batch of observations x

    This computation will take into account x and the previously
    observed historic data.
    The Mahalanobis function is already implemented in the scipy
    environment but it is only applicable between two 1-D arrays.

    Parameters
    ----------

    x: pd.Dataframe.
        Array (a batch) containing the new observations.
    data : pd.Dataframe.
        Array containing of the distribution from which Mahalanobis
        distance of each observation of x is to be computed.
        If none ,a covariance matrix and a mean has to be fed into
        the function.
    cov: np.ndarray.
        Covariance matrix (p x p) of the distribution. If None, will
        be computed from data.
    mean: float.
        If none, will be computed from data.

    Return
    ------
    mahal.diagonal: np.ndarray
        Filtered data array. The returned array contains the mahalanobis
        distance for each observation.
    """

    if (cov is None) and (mean is None) and (data is None):
        raise ValueError('Variables mean, cov and data cannot be None at the same time ')

    if cov is None:
        if isinstance(data, pd.DataFrame):
            cov = np.cov(data.values.T)
        elif isinstance(data, np.ndarray):
            cov = np.cov(data.T)
    if mean is None:
        mean = np.mean(data)

    x_minus_mu = x - mean
    inv_covmat = np.linalg.inv(cov)
    left_term = np.dot(x_minus_mu, inv_covmat)
    mahal = np.dot(left_term, x_minus_mu.T)

    return mahal.diagonal()
