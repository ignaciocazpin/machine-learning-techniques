from scipy.stats import chi2

from .mahalanobis import mahalanobis


def outliers_extraction(x, method='Mahalanobis', mean=None, cov=None, data=None):
    """
    This function detects the outliers present in a multi-dimensional
    incoming dataflow and returns a filtered (without outliers) array.

    Parameters
    ----------
    x: pandas dataframe.
       Array (a batch) containing the new observations.

    data : pandas dataframe, optional
           Array containing of the distribution from which Mahalanobis
           distance of each observation of x is to be computed.
           If none,a covariance matrix and a mean has to be fed into
           the function.
           Default: None

    cov  : numpy array, optional
           Covariance matrix (p x p) of the distribution.
           If None, will be computed from data. Default None

    mean : float.
        If none, will be computed from data. Default: None
        def_f: Natural: Chi2 shape parameter. Default:4

    method: string. Method to be applied in order to identify the outliers.
        Default: Mahalanobis

    Return
    ------
    filtered_data: numpy array.
        Filtered signal
    """

    if method == 'Mahalanobis':
        # We create a cpy of the incoming data
        filtered_data = x.copy()
        # We add a column containing the result of the computation of mahalanobis distance
        filtered_data['mahalanobis'] = mahalanobis(filtered_data, mean, cov, data)
        # We compute the likelihood of the different obtained results
        filtered_data['p_value'] = 1 - chi2.cdf(filtered_data['mahalanobis'],
                                                16)
        # We find candidates to be
        outliers = filtered_data.loc[filtered_data.p_value < 0.15]
        outliers_index = outliers.index.tolist()
        filtered_data.drop(outliers_index, inplace=True)
    else:
        raise ValueError('The given distance calculation method is not supported')

    if filtered_data.shape[0] < 1 / 2 * data.shape[0]:
        raise ValueError('High outliers density, the number of outliers is greater'
                         'than the 50% of incoming observations')

    return filtered_data.drop(columns=['p_value', 'mahalanobis'])
